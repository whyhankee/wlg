import events from 'events';
import { strict as assert } from 'assert';

import moment from 'moment-timezone';
import 'mocha';

import {
  Logger,
  LoggerObj,
  LogTransport,
  TransportConsole,
  logLevel,
  LogExtraTypes,
  logLevelByStr,
  logFormatJson,
  logFormatCompact,
} from './wlg';

describe('Logging', testBasicLogging);
describe('Log flushing', testFlush);
describe('Formatters', testFormatters);

class TransportEvent extends LogTransport {
  myEvents = new events.EventEmitter();

  write(obj: LoggerObj): Promise<void> {
    return new Promise((resolve) => {
      this.myEvents.emit('log', obj);
      return resolve();
    });
  }
}

class TransportDelay extends LogTransport {
  write(): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, 300);
    });
  }
}

function testBasicLogging() {
  it('logs with default transports', () => {
    const l = new Logger(
      { service: 'test' },
      {
        transports: {
          baseTransport: new LogTransport(logLevel.info, {}),
          consoleCompact: new TransportConsole(logLevel.info, {
            formatter: logFormatCompact,
          }),
          consoleJson: new TransportConsole(logLevel.info, {
            formatter: logFormatJson,
          }),
        },
      },
    );
    const transportKeys = l.transports();
    assert.deepEqual(transportKeys, ['baseTransport', 'consoleCompact', 'consoleJson']);

    const numTransports = l.info('some log msg');
    assert.equal(numTransports, 3);
  });

  it('logs proper info and context', async () => {
    const logMsg = 'log message';
    const logState = { state: 'someState' };
    const logExtra = { extra: 'arg' };
    const initContext = { init: 'initContext' };

    const l = new Logger(initContext, { state: logState });
    let logObj = await logData(l, logMsg, logExtra);
    assert.equal(logObj.level, 30);
    assert.equal(logObj.time.getDate(), new Date().getDate());
    assert.equal(logObj.message, logMsg);
    assert.equal(logObj.context, initContext);
    assert.equal(logObj.extra, logExtra);
    assert.equal(logObj.exception, undefined);
    assert.equal(logObj.state, logState);

    l.contextAdd({ more: 'moreContext' });
    logObj = await logData(l, logMsg, logExtra);
    assert.deepEqual(Object.keys(logObj.context), ['init', 'more']);
    assert.equal(logObj.context.init, 'initContext');
    assert.equal(logObj.context.more, 'moreContext');

    l.stateAdd({ moreState: 'moreState' });
    logObj = await logData(l, logMsg, logExtra);
    assert.deepEqual(Object.keys(logObj.state), ['state', 'moreState']);
    assert.equal(logObj.state.state, 'someState');
    assert.equal(logObj.state.moreState, 'moreState');
  });

  it('logs the proper levels', () => {
    const l = new Logger(
      { service: 'test' },
      {
        transports: {
          shortConsole: new TransportConsole(logLevel.debug, {
            formatter: logFormatCompact,
          }),
        },
      },
    );
    const msg = 'nothing relevant';
    const extra = { something: 'extra' };

    assert.equal(l.trace(msg, extra), 0);
    assert.equal(l.debug(msg, extra), 1);
  });

  it('converts string logLevels', () => {
    assert.equal(logLevelByStr('info'), logLevel.info);
    assert.equal(logLevelByStr('fatal'), logLevel.fatal);
  });

  it('logs something to our TransportTest', (done) => {
    const logMsg = 'something else';
    const transport = new TransportEvent(logLevel.info);
    transport.myEvents.once('log', (event: LoggerObj) => {
      assert.equal(event.message, logMsg);
      return done();
    });

    const l = new Logger({ service: 'test' }, { transports: { transport } });
    const numTransports = l.info(logMsg);
    assert.equal(numTransports, 1);
  });

  it('logs an exception', (done) => {
    const logMsg = 'some throw exception';
    const transport = new TransportEvent(logLevel.error);
    transport.myEvents.once('log', (event: LoggerObj) => {
      assert.equal(event.message, logMsg);
      assert(event.exception?.message, 'a thrown exception');
      return done();
    });

    const l = new Logger({ service: 'test' }, { transports: { transport } });
    const numTransports = l.error(logMsg, new Error('a thrown exception'));
    assert.equal(numTransports, 1);
  });

  it('logs using a child-logger', (done) => {
    const timeNow = Date.now();
    const logMsg = 'child logger with timeNow';

    const transport = new TransportEvent(logLevel.info);
    transport.myEvents.once('log', (event: LoggerObj) => {
      assert.equal(event.context.timeNow, timeNow);
      assert.equal(event.message, logMsg);
      return done();
    });

    const l = new Logger({ service: 'test' }, { transports: { transport } });
    const childLogger = l.child({ timeNow });
    const numTransports = childLogger.info(logMsg);
    assert.equal(numTransports, 1);
  });

  it('transport Add/Remove', async () => {
    const logger = new Logger({ service: 'test' });
    let numTransports = logger.info('something');
    assert.equal(numTransports, 1);

    logger.transportAdd('console2', new TransportConsole(logLevel.info));
    numTransports = logger.info('something');
    assert.equal(numTransports, 2);

    logger.transportRemove('console2');
    numTransports = logger.info('something');
    assert.equal(numTransports, 1);
  });
}

function testFlush() {
  it('flush() wait till transports are done', async () => {
    const logMsg = 'some message';

    // delay transport around 300ms
    const transport = new TransportDelay(logLevel.info);
    const l = new Logger({ service: 'test' }, { transports: { transport } });
    l.info(logMsg);

    // flush, should wait until transported
    const timeNow = Date.now();
    let flushed = await l.flush(1000);
    assert.equal(flushed, true);

    // Time should be just over 300ms
    const timeTaken = Date.now() - timeNow;
    assert.ok(timeTaken > 300 && timeTaken < 400, `proper flush time ${timeTaken}ms`);

    // Flush cannot be done in this small timeframe
    l.info(logMsg);
    flushed = await l.flush(200);
    assert.equal(flushed, false);
  });
}

function testFormatters() {
  const logObj: LoggerObj = {
    time: moment('1999-12-31T23:00:00.000Z').toDate(),
    level: logLevel.trace,
    message: 'some msg',
    context: {
      requestId: '12345',
    },
    state: {
      rq: {
        method: 'GET',
        url: '/something',
        etc: '..',
      },
    },
    extra: {
      some: 'extra_info',
    },
    exception: new Error('some thrown error'),
  };

  it('logFormatJson', () => {
    const result = logFormatJson(logObj);
    const resultObj = JSON.parse(result);
    assert.deepEqual(
      Object.keys(resultObj).sort(),
      ['level', 'message', 'requestId', 'extra', 'time', 'exception'].sort(),
    );
    assert.equal(resultObj.level, 'trace');
    assert.equal(resultObj.message, 'some msg');
    assert.equal(resultObj.requestId, '12345');
    assert.deepEqual(resultObj.extra, { some: 'extra_info' });
    assert.equal(resultObj.time, '1999-12-31T23:00:00.000Z');
    assert.equal(resultObj.exception.message, 'some thrown error');

    const stack: string[] = resultObj.exception.stack.split('\n');
    assert.ok(stack.length > 5, `proper stack length ${stack.length}`);
    assert.match(stack[0], /Error: some thrown error/);
  });

  it('logFormatCompact', () => {
    const result = logFormatCompact(logObj);
    assert.match(
      result,
      /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z trace: some msg {"context":{.*"}}/,
    );
    const matchResults = result.match(/(\{.*})/);
    assert.ok(matchResults);
    assert.equal(matchResults.length, 2);
    const parsed = JSON.parse(matchResults[0]);
    assert.deepEqual(
      Object.keys(parsed).sort(),
      ['context', 'extra', 'exception'].sort(),
    );
    assert.deepEqual(parsed.context, { requestId: '12345' });
    assert.deepEqual(parsed.extra, { some: 'extra_info' });
    assert.equal(parsed.exception.message, 'some thrown error');
    assert.match(parsed.exception.stack, /Error: some thrown error/);
  });
}

/**
 * Returns the logged data-structure for any logger call
 * @param l the logger to log on
 * @param msg the message to log
 * @param extra extra data to log
 */
async function logData(l: Logger, msg: string, extra: LogExtraTypes): Promise<LoggerObj> {
  return new Promise((resolve) => {
    const transport = new TransportEvent(logLevel.info);
    transport.myEvents.once('log', (event: LoggerObj) => {
      l.transportRemove('getData');
      return resolve(event);
    });

    l.transportAdd('getData', transport);
    l.info(msg, extra);
  });
}
