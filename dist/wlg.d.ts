export declare enum logLevel {
    trace = 10,
    debug = 20,
    verbose = 25,
    info = 30,
    warn = 40,
    error = 50,
    fatal = 60
}
interface ILoggerConfig {
    parent?: Logger;
    state?: LogRecord;
    transports?: ILoggerTransports;
}
interface ILoggerTransports {
    [key: string]: LogTransport;
}
export declare type LogRecord = Record<string, any>;
export declare type LogExtraTypes = LogRecord | Error;
export interface LoggerObj {
    time: Date;
    level: logLevel;
    exception?: Error;
    message: string;
    context: LogRecord;
    state: LogRecord;
    extra?: LogRecord;
}
export declare class Logger {
    private readonly context;
    private readonly state;
    private readonly parent;
    private readonly usedTransports;
    private inTransport;
    /**
     * Construct a new logger
     * @param ctx context for the logger
     * @param cfg configuration for the logger
     */
    constructor(ctx?: LogRecord, cfg?: ILoggerConfig);
    /**
     * returns the attached transports for the logger
     */
    transports(): string[];
    /**
     * Add context to an existing logger
     */
    contextAdd(add: LogRecord): void;
    /**
     * Add State to an existing logger
     */
    stateAdd(add: LogRecord): void;
    /**
     * Add transport to a logger
     * @param name name of transport to add
     * @param transport configured transport
     */
    transportAdd(name: string, transport: LogTransport): void;
    /**
     * Removes a configured transport
     * @param name name of transport to remove
     */
    transportRemove(name: string): void;
    private _transport;
    trace(str: string, extra?: LogExtraTypes): number;
    debug(str: string, extra?: LogExtraTypes): number;
    verbose(str: string, extra?: LogExtraTypes): number;
    info(str: string, extra?: LogExtraTypes): number;
    warn(str: string, extra?: LogExtraTypes): number;
    error(str: string, extra?: LogExtraTypes): number;
    fatal(str: string, extra?: LogExtraTypes): number;
    /**
     * Creates a new child-logger, context will be merged with existing context
     * @param ctx properties to add to the current log-context
     * @param state state properties to add to the current list
     */
    child(ctx: LogRecord, state?: LogRecord): Logger;
    /**
     * Make sure all transports are flushed
     * @param expireMs time
     * @returns indicator for success
     */
    flush(expireMs: number): Promise<boolean>;
}
/**
 * Convert a string variable to a logLevel enum value
 * @param level string value for the given loglevel
 * @throws if the logLevel cannot be found
 * @returns logLevel enum value (number)
 */
export declare function logLevelByStr(level: string): logLevel;
interface LogFormatter {
    (obj: LoggerObj): string;
}
/**
 * Formats in compact mode
 * @param obj logObj to format
 */
export declare function logFormatCompact(obj: LoggerObj): string;
/**
 * Formats to JSON format
 * @param obj logObj to format
 */
export declare function logFormatJson(obj: LoggerObj): string;
export declare function jsonReplacer(_: string, value: unknown): any;
/**
 * transports
 */
export interface LogTransportCfg {
    formatter?: LogFormatter;
}
/**
 * LogTransport - Base class for other transports
 * @param level
 * @param cfg
 */
export declare class LogTransport {
    minLevel: number;
    formatter: typeof logFormatJson;
    constructor(level: number, cfg?: LogTransportCfg);
    levelIncludes(level: number): boolean;
    write(_: LoggerObj): Promise<void>;
}
/**
 * Transport for Console output
 */
export declare class TransportConsole extends LogTransport {
    write(obj: LoggerObj): Promise<void>;
}
export {};
