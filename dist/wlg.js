"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransportConsole = exports.LogTransport = exports.jsonReplacer = exports.logFormatJson = exports.logFormatCompact = exports.logLevelByStr = exports.Logger = exports.logLevel = void 0;
var logLevel;
(function (logLevel) {
    logLevel[logLevel["trace"] = 10] = "trace";
    logLevel[logLevel["debug"] = 20] = "debug";
    logLevel[logLevel["verbose"] = 25] = "verbose";
    logLevel[logLevel["info"] = 30] = "info";
    logLevel[logLevel["warn"] = 40] = "warn";
    logLevel[logLevel["error"] = 50] = "error";
    logLevel[logLevel["fatal"] = 60] = "fatal";
})(logLevel = exports.logLevel || (exports.logLevel = {}));
class Logger {
    /**
     * Construct a new logger
     * @param ctx context for the logger
     * @param cfg configuration for the logger
     */
    constructor(ctx, cfg) {
        this.context = {};
        this.state = {};
        this.parent = null;
        this.usedTransports = {};
        this.inTransport = 0;
        this.parent = (cfg === null || cfg === void 0 ? void 0 : cfg.parent) || null;
        this.state = (cfg === null || cfg === void 0 ? void 0 : cfg.state) || {};
        this.context = ctx || {};
        if (!this.parent) {
            this.usedTransports = (cfg === null || cfg === void 0 ? void 0 : cfg.transports) || {
                console: new TransportConsole(logLevel.info),
            };
        }
    }
    /**
     * returns the attached transports for the logger
     */
    transports() {
        return Object.keys(this.usedTransports).sort();
    }
    /**
     * Add context to an existing logger
     */
    contextAdd(add) {
        Object.keys(add).forEach((k) => {
            this.context[k] = add[k];
        });
    }
    /**
     * Add State to an existing logger
     */
    stateAdd(add) {
        Object.keys(add).forEach((k) => {
            this.state[k] = add[k];
        });
    }
    /**
     * Add transport to a logger
     * @param name name of transport to add
     * @param transport configured transport
     */
    transportAdd(name, transport) {
        if (this.usedTransports[name]) {
            throw new Error(`transportAdd: already has a transport with name: ${name}`);
        }
        this.usedTransports[name] = transport;
    }
    /**
     * Removes a configured transport
     * @param name name of transport to remove
     */
    transportRemove(name) {
        if (!this.usedTransports[name]) {
            throw new Error(`transportRemove: no transport with name: ${name}`);
        }
        delete this.usedTransports[name];
    }
    _transport(level, message, extra, context, state) {
        if (this.parent) {
            return this.parent._transport(level, message, extra, context, state);
        }
        // Transform possible error object
        let exception;
        if (extra instanceof Error) {
            exception = extra;
            extra = {};
        }
        const logObj = {
            level,
            time: new Date(),
            exception,
            message,
            context,
            state,
            extra,
        };
        const transportPromises = Object.keys(this.usedTransports)
            .filter((tranportKey) => {
            return this.usedTransports[tranportKey].levelIncludes(logObj.level);
        })
            .map((transportKey) => {
            return this.usedTransports[transportKey].write(logObj);
        });
        this.inTransport++;
        Promise.all(transportPromises).then(() => {
            this.inTransport--;
        });
        return transportPromises.length;
    }
    trace(str, extra) {
        return this._transport(logLevel.trace, str, extra, this.context, this.state);
    }
    debug(str, extra) {
        return this._transport(logLevel.debug, str, extra, this.context, this.state);
    }
    verbose(str, extra) {
        return this._transport(logLevel.verbose, str, extra, this.context, this.state);
    }
    info(str, extra) {
        return this._transport(logLevel.info, str, extra, this.context, this.state);
    }
    warn(str, extra) {
        return this._transport(logLevel.warn, str, extra, this.context, this.state);
    }
    error(str, extra) {
        return this._transport(logLevel.error, str, extra, this.context, this.state);
    }
    fatal(str, extra) {
        return this._transport(logLevel.fatal, str, extra, this.context, this.state);
    }
    /**
     * Creates a new child-logger, context will be merged with existing context
     * @param ctx properties to add to the current log-context
     * @param state state properties to add to the current list
     */
    child(ctx, state) {
        return new Logger(Object.assign(Object.assign({}, this.context), ctx), {
            parent: this,
            state: Object.assign(Object.assign({}, this.state), state),
        });
    }
    /**
     * Make sure all transports are flushed
     * @param expireMs time
     * @returns indicator for success
     */
    flush(expireMs) {
        return __awaiter(this, void 0, void 0, function* () {
            const expires = Date.now() + expireMs;
            const delayMs = 50;
            if (this.inTransport === 0) {
                return true;
            }
            while (Date.now() < expires) {
                yield _sleep(delayMs);
                if (this.inTransport === 0) {
                    return true;
                }
            }
            return false;
        });
    }
}
exports.Logger = Logger;
/**
 * Convert a string variable to a logLevel enum value
 * @param level string value for the given loglevel
 * @throws if the logLevel cannot be found
 * @returns logLevel enum value (number)
 */
function logLevelByStr(level) {
    const matchedLevels = Object.entries(logLevel).filter((a) => a[0] === level);
    if (matchedLevels.length === 0) {
        throw new Error(`logLevelByStr: unknown logLevel ${level}`);
    }
    return matchedLevels[0][1];
}
exports.logLevelByStr = logLevelByStr;
/**
 * Formats in compact mode
 * @param obj logObj to format
 */
function logFormatCompact(obj) {
    let logStr = `${obj.time.toISOString()} ${logLevel[obj.level]}: ${obj.message}`;
    const x = {};
    if (obj.context && Object.keys(obj.context).length)
        x.context = obj.context;
    if (obj.extra && Object.keys(obj.extra).length)
        x.extra = obj.extra;
    if (obj.exception)
        x.exception = obj.exception;
    if (x.context || x.extra || x.exception) {
        logStr += ` ${JSON.stringify(x, jsonReplacer)}`;
    }
    return logStr;
}
exports.logFormatCompact = logFormatCompact;
/**
 * Formats to JSON format
 * @param obj logObj to format
 */
function logFormatJson(obj) {
    // state is unused, should not be shown
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { context, extra, level, state } = obj, o = __rest(obj, ["context", "extra", "level", "state"]);
    const levelStr = logLevel[level];
    const logObj = Object.assign(Object.assign(Object.assign({ level: levelStr }, o), context), { extra });
    return JSON.stringify(logObj, jsonReplacer);
}
exports.logFormatJson = logFormatJson;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function jsonReplacer(_, value) {
    if (value instanceof Error) {
        const error = {};
        Object.getOwnPropertyNames(value).forEach((key) => {
            error[key] = value[key];
        });
        return error;
    }
    return value;
}
exports.jsonReplacer = jsonReplacer;
/**
 * LogTransport - Base class for other transports
 * @param level
 * @param cfg
 */
class LogTransport {
    constructor(level, cfg) {
        this.minLevel = logLevel.info;
        this.formatter = logFormatJson;
        this.minLevel = level;
        if (cfg && cfg.formatter) {
            this.formatter = cfg.formatter;
        }
    }
    levelIncludes(level) {
        return level >= this.minLevel;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    write(_) {
        return Promise.resolve();
    }
}
exports.LogTransport = LogTransport;
/**
 * Transport for Console output
 */
class TransportConsole extends LogTransport {
    write(obj) {
        return new Promise((resolve) => {
            const fmtStr = this.formatter(obj);
            // eslint-disable-next-line no-console
            console.log(fmtStr);
            return resolve();
        });
    }
}
exports.TransportConsole = TransportConsole;
function _sleep(ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    });
}
//# sourceMappingURL=wlg.js.map