# wlg - NodeJS/Deno logger suite

[![pipeline status](https://gitlab.com/whyhankee/wlg/badges/master/pipeline.svg)](https://gitlab.com/whyhankee/wlg/-/commits/master)
[![coverage report](https://gitlab.com/whyhankee/wlg/badges/master/coverage.svg)](https://gitlab.com/whyhankee/wlg/-/commits/master)

## Features

- Small, easy to understand and easy to use
- Javascript/Typescript compatible
- Context properties (e.g. servicename, requestId, etc)
- State properties (e.g. request-data). Not logged, usable by Transports
- Dynamically add/remove transports
- Child-loggers
- Async transports
- Flush() method (for at the end of your lambdas)

## Setup and basic logging

```javascript
import { Logger, logLevel, TransportConsole, logFormatCompact } from 'wlg';

const log = new Logger(
  { service: 'test' },
  {
    transports: {
      shortConsole: new TransportConsole(logLevel.debug, {
        formatter: logFormatCompact,
      }),
    },
  },
);

// basis usage
log.info('some string', { extra_opt1: 'something_else' });

// child logging
//  requestId becomes context (and is printed, logged)
//  rq: becomes state (not printed, but available for transports)
childLogger = log.child({ requestId: 'my-request-id' }, { rq: ctx.rq });
childLogger.info('some string for the request', { extra: 'args' });

// Flush transports before closing (a lambda)
await log.flush(1000);
```

## API references for the Logger class

- `.trace|debug|verbose|info|warn|error|fatal(msg, obj|Error)`

Logs a message with optional extra|error properties.

- `.transports()`

Returns list of transport-names currently in use by the logger

- `.transportAdd(name, transport)`

Add a new transport to the Logger, will throw if a transport with that name already exists.

- `.transportRemove(name)`

Remove a transport from the Logger

- `.contextSet(obj)`

Adds `obj` to the logger context

- `.stateAdd(obj)`

Adds `obj` to the logger state

- `.child(objContext, objState)`

Create a child logger with extra context properties (objContext) and state (objState)
When a line gets logged through the child logger the state and context of the child logger is merged with the main logger.

## API Reference for other exported data

- `logLevelByStr(string)`

Returns logLevel enum value for given string.
Will throw on unknown loglevel.

- `logLevel`

Enum with all the loglevels

- `LogTransport`

Base LogTransport implementation

- `TransportConsole`

Basic console output transport

- `logFormatCompact`

Compact formatter

- `logFormatJson`

JSON formatter
