export enum logLevel {
  trace = 10,
  debug = 20,
  verbose = 25,
  info = 30,
  warn = 40,
  error = 50,
  fatal = 60,
}

interface ILoggerConfig {
  parent?: Logger;
  state?: LogRecord;
  transports?: ILoggerTransports;
}

interface ILoggerTransports {
  [key: string]: LogTransport;
}

/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export type LogRecord = Record<string, any>;
export type LogExtraTypes = LogRecord | Error;

export interface LoggerObj {
  time: Date;
  level: logLevel;
  exception?: Error;
  message: string;
  context: LogRecord;
  state: LogRecord;
  extra?: LogRecord;
}

export class Logger {
  private readonly context: LogRecord = {};
  private readonly state: LogRecord = {};
  private readonly parent: Logger | null = null;
  private readonly usedTransports: ILoggerTransports = {};
  private inTransport = 0;

  /**
   * Construct a new logger
   * @param ctx context for the logger
   * @param cfg configuration for the logger
   */
  constructor(ctx?: LogRecord, cfg?: ILoggerConfig) {
    this.parent = cfg?.parent || null;
    this.state = cfg?.state || {};
    this.context = ctx || {};

    if (!this.parent) {
      this.usedTransports = cfg?.transports || {
        console: new TransportConsole(logLevel.info),
      };
    }
  }

  /**
   * returns the attached transports for the logger
   */
  transports(): string[] {
    return Object.keys(this.usedTransports).sort();
  }

  /**
   * Add context to an existing logger
   */
  contextAdd(add: LogRecord): void {
    Object.keys(add).forEach((k) => {
      this.context[k] = add[k];
    });
  }

  /**
   * Add State to an existing logger
   */
  stateAdd(add: LogRecord): void {
    Object.keys(add).forEach((k) => {
      this.state[k] = add[k];
    });
  }

  /**
   * Add transport to a logger
   * @param name name of transport to add
   * @param transport configured transport
   */
  transportAdd(name: string, transport: LogTransport): void {
    if (this.usedTransports[name]) {
      throw new Error(`transportAdd: already has a transport with name: ${name}`);
    }
    this.usedTransports[name] = transport;
  }

  /**
   * Removes a configured transport
   * @param name name of transport to remove
   */
  transportRemove(name: string): void {
    if (!this.usedTransports[name]) {
      throw new Error(`transportRemove: no transport with name: ${name}`);
    }
    delete this.usedTransports[name];
  }

  private _transport(
    level: number,
    message: string,
    extra: Error | LogRecord | undefined,
    context: LogRecord,
    state: LogRecord,
  ): number {
    if (this.parent) {
      return this.parent._transport(level, message, extra, context, state);
    }

    // Transform possible error object
    let exception: Error | undefined;
    if (extra instanceof Error) {
      exception = extra;
      extra = {};
    }

    const logObj: LoggerObj = {
      level,
      time: new Date(),
      exception,
      message,
      context,
      state,
      extra,
    };
    const transportPromises = Object.keys(this.usedTransports)
      .filter((tranportKey) => {
        return this.usedTransports[tranportKey].levelIncludes(logObj.level);
      })
      .map((transportKey) => {
        return this.usedTransports[transportKey].write(logObj);
      });

    this.inTransport++;
    Promise.all(transportPromises).then(() => {
      this.inTransport--;
    });
    return transportPromises.length;
  }

  trace(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.trace, str, extra, this.context, this.state);
  }
  debug(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.debug, str, extra, this.context, this.state);
  }
  verbose(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.verbose, str, extra, this.context, this.state);
  }
  info(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.info, str, extra, this.context, this.state);
  }
  warn(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.warn, str, extra, this.context, this.state);
  }
  error(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.error, str, extra, this.context, this.state);
  }
  fatal(str: string, extra?: LogExtraTypes): number {
    return this._transport(logLevel.fatal, str, extra, this.context, this.state);
  }

  /**
   * Creates a new child-logger, context will be merged with existing context
   * @param ctx properties to add to the current log-context
   * @param state state properties to add to the current list
   */
  child(ctx: LogRecord, state?: LogRecord): Logger {
    return new Logger(
      { ...this.context, ...ctx },
      {
        parent: this,
        state: { ...this.state, ...state },
      },
    );
  }

  /**
   * Make sure all transports are flushed
   * @param expireMs time
   * @returns indicator for success
   */
  async flush(expireMs: number): Promise<boolean> {
    const expires = Date.now() + expireMs;
    const delayMs = 50;

    if (this.inTransport === 0) {
      return true;
    }

    while (Date.now() < expires) {
      await _sleep(delayMs);
      if (this.inTransport === 0) {
        return true;
      }
    }
    return false;
  }
}

/**
 * Convert a string variable to a logLevel enum value
 * @param level string value for the given loglevel
 * @throws if the logLevel cannot be found
 * @returns logLevel enum value (number)
 */
export function logLevelByStr(level: string): logLevel {
  const matchedLevels = Object.entries(logLevel).filter((a) => a[0] === level);
  if (matchedLevels.length === 0) {
    throw new Error(`logLevelByStr: unknown logLevel ${level}`);
  }
  return matchedLevels[0][1] as logLevel;
}

interface LogFormatter {
  (obj: LoggerObj): string;
}

/**
 * Formats in compact mode
 * @param obj logObj to format
 */
export function logFormatCompact(obj: LoggerObj): string {
  let logStr = `${obj.time.toISOString()} ${logLevel[obj.level]}: ${obj.message}`;
  const x: LogRecord = {};

  if (obj.context && Object.keys(obj.context).length) x.context = obj.context;
  if (obj.extra && Object.keys(obj.extra).length) x.extra = obj.extra;
  if (obj.exception) x.exception = obj.exception;

  if (x.context || x.extra || x.exception) {
    logStr += ` ${JSON.stringify(x, jsonReplacer)}`;
  }
  return logStr;
}

/**
 * Formats to JSON format
 * @param obj logObj to format
 */
export function logFormatJson(obj: LoggerObj): string {
  // state is unused, should not be shown
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { context, extra, level, state, ...o } = obj;
  const levelStr = logLevel[level];
  const logObj = {
    level: levelStr,
    ...o,
    ...context,
    extra,
  };
  return JSON.stringify(logObj, jsonReplacer);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function jsonReplacer(_: string, value: unknown): any {
  if (value instanceof Error) {
    const error: LogRecord = {};
    Object.getOwnPropertyNames(value).forEach((key) => {
      error[key] = (value as LogRecord)[key];
    });
    return error;
  }

  return value;
}

/**
 * transports
 */
export interface LogTransportCfg {
  formatter?: LogFormatter;
}

/**
 * LogTransport - Base class for other transports
 * @param level
 * @param cfg
 */
export class LogTransport {
  minLevel: number = logLevel.info;
  formatter = logFormatJson;

  constructor(level: number, cfg?: LogTransportCfg) {
    this.minLevel = level;
    if (cfg && cfg.formatter) {
      this.formatter = cfg.formatter;
    }
  }

  levelIncludes(level: number): boolean {
    return level >= this.minLevel;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  write(_: LoggerObj): Promise<void> {
    return Promise.resolve();
  }
}

/**
 * Transport for Console output
 */
export class TransportConsole extends LogTransport {
  write(obj: LoggerObj): Promise<void> {
    return new Promise((resolve) => {
      const fmtStr = this.formatter(obj);
      // eslint-disable-next-line no-console
      console.log(fmtStr);
      return resolve();
    });
  }
}

async function _sleep(ms: number) {
  return new Promise((resolve): void => {
    setTimeout(resolve, ms);
  });
}
